#!/bin/bash

if [[ ! -z ${MAAP_PY_GIT} ]]; then
    echo "Value provided for MAAP_PY_GIT: ${MAAP_PY_GIT}"
    if [[ ! -z ${MAAP_PY_TAG} ]]; then
        echo "Installing version ${MAAP_PY_TAG}"
        git clone --single-branch --branch ${MAAP_PY_TAG} ${MAAP_PY_GIT}
    else
        echo "Installing default branch"
        git clone ${MAAP_PY_GIT}
    fi
    pushd maap-py
    pip install -e .
    echo "Setting MAAP_CONF to ${PWD}"
    MAAP_CONF=${PWD}
    popd
fi

echo "No value provided for MAAP_PY_GIT and MAAP_PY_TAG, not installing maap-py"
