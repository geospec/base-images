#!/bin/bash

set -ex
base_image_dir=$(dirname $0)
BRANCH=${CI_COMMIT_REF_NAME}
DIRS="isofit jupyterlab3"
if [[ ! -z "$@" ]]; then
    DIRS=$@
fi
for dir in ${DIRS}; do
    pushd $base_image_dir/$dir
    IMAGE_NAME=$(basename $dir)
    IMAGE_REF=${CUSTOM_CI_REGISTRY_IMAGE}/base_images/${IMAGE_NAME}:${BRANCH}
    docker build -t ${IMAGE_REF} --build-arg IMAGE_REF=${IMAGE_REF} -f docker/Dockerfile .
    docker push ${IMAGE_REF}
    if [[ ! -z "${CI_COMMIT_TAG// }" ]]; then
      IMAGE_TAG_NAME=${CUSTOM_CI_REGISTRY_IMAGE}/base_images/${IMAGE_NAME}:${CI_COMMIT_TAG}
      docker tag ${IMAGE_REF} ${IMAGE_TAG_NAME}
      docker push ${IMAGE_TAG_NAME}
    fi
    if [[ ! -z ${CI_REGISTRY} ]]; then
        REGISTRY_IMAGE_REF=${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${BRANCH}
        docker tag ${IMAGE_REF} ${REGISTRY_IMAGE_REF}
        docker push ${REGISTRY_IMAGE_REF}
    fi
    popd
    echo "$IMAGE_REF" >> built_images.txt
done
